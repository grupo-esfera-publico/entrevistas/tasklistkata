import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';


export interface ICommandLineInterface {
    readLine(): Promise<string>;
    writeLine(ln: string): void;
    close(): void;
}

export class CommandLineInterface implements ICommandLineInterface {

    private _readline: readline.Interface;

    constructor() {
        this._readline = readline.createInterface({ input, output });

        this._readline.on('close', () => {
            output.end();
        });
    }

    public async readLine(): Promise<string> {
        const commandInput = await this._readline.question('> ');
        return commandInput;
    }

    public writeLine(ln: string): void {
        console.log(ln);
    }

    public close(): void {
        this._readline.close();
    }
}