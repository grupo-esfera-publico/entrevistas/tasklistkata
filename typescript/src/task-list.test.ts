import { CommandLineInterface, ICommandLineInterface } from "./command-line-interface";
import { TaskList } from "./task-list";

describe('Task List tests', () => {

    let taskList: TaskList;
    let cli: ICommandLineInterface;

    beforeEach(()=>{
        cli = new CommandLineInterface();
        taskList = new TaskList(cli);
        jest.resetAllMocks();
        jest.spyOn(cli, 'close').mockImplementation(()=>{});
    });

    it('should check that this is working', () => {
      expect(true).toBe(true)
    });

    it('should show an empty task list', async () => {
        jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(writeLineSpy).not.toHaveBeenCalled();

    });

    it('should add a project', async () => {
        var readMock = jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("add project TestProject"))
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(readMock).toHaveBeenCalledTimes(3);
        expect(writeLineSpy).toHaveBeenCalledWith("TestProject");

    });

    it('should add a task', async () => {
        jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("add project TestProject"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject TestTask"))
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(writeLineSpy.mock.calls).toEqual([
            ["TestProject"],
            ["      1: TestTask"], 
            [""]]);

    });

    it('should add 2 tasks to the same project', async () => {
        jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("add project TestProject"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject TestTask"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject Second Task"))
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(writeLineSpy.mock.calls).toEqual([
            ["TestProject"],
            ["      1: TestTask"],
            ["      2: Second Task"], 
            [""]]);

    });

    it('should mark a task as done', async () => {
        jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("add project TestProject"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject TestTask"))
                    .mockReturnValueOnce(Promise.resolve("check 1"))
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(writeLineSpy.mock.calls).toEqual([
            ["TestProject"],
            ["    x 1: TestTask"], 
            [""]]);

    });

    it('should uncheck a task', async () => {
        jest.spyOn(cli, "readLine")
                    .mockReturnValueOnce(Promise.resolve("add project TestProject"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject Test Task"))
                    .mockReturnValueOnce(Promise.resolve("add task TestProject Second Task"))
                    .mockReturnValueOnce(Promise.resolve("check 1"))
                    .mockReturnValueOnce(Promise.resolve("check 2"))
                    .mockReturnValueOnce(Promise.resolve("uncheck 1"))
                    .mockReturnValueOnce(Promise.resolve("show"))
                    .mockReturnValueOnce(Promise.resolve("quit"));
                    
        var writeLineSpy = jest.spyOn(cli, "writeLine");
        await taskList.start();

        expect(writeLineSpy.mock.calls).toEqual([
            ["TestProject"],
            ["      1: Test Task"],
            ["    x 2: Second Task"], 
            [""]]);

    });
});

  