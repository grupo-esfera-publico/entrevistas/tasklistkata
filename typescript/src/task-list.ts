import { CommandLineInterface, ICommandLineInterface } from "./command-line-interface";
import { Task } from "./task.model";

function splitFirstSpace(s: string) {
    var pos = s.indexOf(' ');
    if(pos === -1) {
        return [s];
    }
    return [s.substr(0, pos), s.substr(pos+1)]
}

export class TaskList {

    private commandLineInterface: ICommandLineInterface;

    static QUIT = 'quit';
    private tasks: {[index: string]: Task[]} = {};
    private lastId = 0;

    constructor(cli?: ICommandLineInterface){
        if (cli) {
            this.commandLineInterface = cli;

        } else {
            this.commandLineInterface = new CommandLineInterface();
        }
    }

    public async start() {
        do {
            var result = await this.processLine();
        } while(result);
    }

    public async processLine() : Promise<boolean> {
        var cmd = await this.commandLineInterface.readLine();
        if (cmd != TaskList.QUIT) {
            this.execute(cmd);
            return true;

        } else {
            this.commandLineInterface.close();
        }

        return false;
    }

    public execute(commandLine: string) {
        var commandRest = splitFirstSpace(commandLine);
        var command = commandRest[0];
        switch (command) {
            case "show":
                this.show();
                break;
            case "add":
                this.add(commandRest[1]);
                break;
            case "check":
                this.check(commandRest[1]);
                break;
            case "uncheck":
                this.uncheck(commandRest[1]);
                break;
            case "help":
                this.help();
                break;
            default:
                this.error(command);
                break;
        }
    }

    forEachProject(func: (key: string, value: Task[]) => any) {
        for(var key in this.tasks) {
            if(this.tasks.hasOwnProperty(key))
                func(key, this.tasks[key])
        }
    }


    private show() {
        this.forEachProject((project, taskList) => {
            this.commandLineInterface.writeLine(project);
            taskList.forEach((task) => {
                this.commandLineInterface.writeLine("    " + (task.done ? 'x' : ' ') + " " + task.id + ": " + task.description);
            });
            this.commandLineInterface.writeLine('');
        });
    }

    private add(commandLine: string) {
        var subcommandRest = splitFirstSpace(commandLine);
        var subcommand = subcommandRest[0];
        if (subcommand === "project") {
            this.addProject(subcommandRest[1]);
        } else if (subcommand === "task") {
            var projectTask = splitFirstSpace(subcommandRest[1]);
            this.addTask(projectTask[0], projectTask[1]);
        }
    }

    private addProject(name: string) {
        this.tasks[name] = [];
    }

    private addTask(project: string, description: string) {
        var projectTasks = this.tasks[project];
        if (projectTasks == null) {
            this.commandLineInterface.writeLine("Could not find a project with the name " + project);
            return;
        }
        projectTasks.push(new Task(this.nextId(), description, false));
    }

    private check(idString: string) {
        this.setDone(idString, true);
    }

    private uncheck(idString: string) {
        this.setDone(idString, false);
    }

    private setDone(idString: string, done: boolean) {
        var id = parseInt(idString, 10);
        var found = false;
        this.forEachProject((project, taskList) => {
            taskList.forEach((task) => {
                if (task.id == id) {
                    task.done = done;
                    found = true;
                }
            });
        });
        if(!found)
            this.commandLineInterface.writeLine("Could not find a task with an ID of " + id);
    }


    private help() {
        this.commandLineInterface.writeLine("Commands:");
        this.commandLineInterface.writeLine("  show");
        this.commandLineInterface.writeLine("  add project <project name>");
        this.commandLineInterface.writeLine("  add task <project name> <task description>");
        this.commandLineInterface.writeLine("  check <task ID>");
        this.commandLineInterface.writeLine("  uncheck <task ID>");
        this.commandLineInterface.writeLine("");
    }

    private error(command: string) {
        this.commandLineInterface.writeLine('I don\'t know what the command "' + command + '" is.');
    }

    private nextId(): number {
        return ++this.lastId;
    }


}